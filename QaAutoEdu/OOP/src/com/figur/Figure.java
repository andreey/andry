package com.figur;

public abstract class Figure {
    public double x;
    public double y;
    public double area;

    public Figure(double x, double y) {

        this.x = x;
        this.y = y;
    }

    public void moving(double x1, double y1) {
        x =x +x1;
        y =y +y1;
    }
    public abstract void area();

    public abstract void changeSize(double koef);

}

