package com.games;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {

        while (true) {
            try {
                System.out.println("введиет число от 1 до 9 :");
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String n = reader.readLine();
                int a = Integer.parseInt(n);
                if (a >= 1 && a <= 9) {
                    int [] [] res = Matrix.Array(a);
                    for (int i = 0; i < res.length; i++) {
                        for (int j = 0; j < res[i].length; j++){
                            System.out.print(res[i][j]);
                            if (j < res[i].length - 1)
                            { System.out.print(" ");
                            }
                        } System.out.println();
                    }
                } else {
                    System.out.println("неверное число");
                    continue;
                }
                System.out.println("чтобы выйти введи слово exit. чтобы повторить введи любую клавишу");
                String x = reader.readLine();
                if (x.equals("exit")) {
                    System.out.println("пока");
                    break;
                }

            } catch (NumberFormatException e) {
                System.out.println("введи число");
            }
        }
        Snail test = new Snail();
        System.out.println("построение двумерного массива в виде улитки");
        System.out.println("введите число равное 3 или больше:");
        while(true) {
            BufferedReader reader1 = new BufferedReader(new InputStreamReader(System.in));
            String r = reader1.readLine();
            int vb = Integer.parseInt(r);
            if(vb<3){
                System.out.println("значение введено меньше 3. повторите ввод");
            }else{
                test.calculateSnail(vb);
                break;
            }
        }
        System.out.println("проверка на Палиндром");
        System.out.println("введите любое слово или строку:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String n = reader.readLine();
        Palindrom test1 = new Palindrom();
        if(n.contains(" ")) {
            test1.checkPhrase(n);
        } else{
            test1.checkWord(n);
        }
    }
}
