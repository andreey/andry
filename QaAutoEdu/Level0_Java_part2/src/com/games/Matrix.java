package com.games;

public class Matrix {
    public static int[][] Array(int ch){
        int[][] arr = new int[ch][ch];
        int x = 1;
        for (int i = 0; i < ch; i++) {
            for (int j = 0; j < ch; j++) {
                arr[i][j] = x;
                if (x >= 9) {
                    x=1;
                } else {
                    x++;

                }
            }

        }
        return arr;
    }
}
