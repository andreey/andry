package com.hello;

import com.welcome.Hello;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Hello hello = new Hello();

        Scanner in = new Scanner(System.in);
        System.out.println("Напиши имя");
        hello.setupName(in.nextLine());

        hello.welcome();
        System.out.println("Заработало");

        hello.byebye();
    }

}
